import Arbitro from "./classes/Arbitro.js"

let arbitro1 = new Arbitro (35,"Braulio da Silva Machado", "Árbitro principal")
let arbitro2 = new Arbitro (36,"Diego Pombo Lopez", "Árbitro principal")
let arbitro3 = new Arbitro (37,"Leandro Pedro Vuaden", "Árbitro principal")
let arbitro4 = new Arbitro (38,"Luiz Flavio de Oliveira", "Árbitro principal")
let arbitro5 = new Arbitro (39,"Edina Alves Batista", "Árbitro principal")
let arbitro6 = new Arbitro (40,"Anderson Jose de Moraes Coelho", "Árbitro principal")
let arbitro7 = new Arbitro (41,"Paulo Cezar Zanovelli da Silva", "Árbitro principal")
let arbitro8 = new Arbitro (42,"Raphael Claus", "Árbitro principal")
let arbitro9 = new Arbitro (43," Flávio Rodrigues de Souza", "Árbitro principal")
let arbitro10 = new Arbitro (44,"Caio Max Augusto Vieira", "Árbitro principal")
let arbitro11 = new Arbitro (45,"Anderson Daronco ", "Árbitro principal")
let arbitro12 = new Arbitro (46,"Ramon Abatti Abel", "Árbitro principal")
let arbitro13 = new Arbitro (47,"Yuri Elino Ferreira da Cruz", "Árbitro principal")
let arbitro14 = new Arbitro (48,"Joao Vitor Gobi", "Árbitro principal")
let arbitro15 = new Arbitro (49," Leandro Pedro Vuaden", "Árbitro principal")
let arbitro16 = new Arbitro (50,"Bruno Arleu de Araujo", "Árbitro principal")

import Estadio from "./classes/Estadio.js"

let estadio1 = new Estadio (51, "Estádio Santa Cruz",  "Ribeirão Preto (SP)", 29.292)
let estadio2 = new Estadio (52, "Estádio Raulino de Oliveira",  "Volta Redonda (RJ)", 18.230)
let estadio3 = new Estadio (53, " Estádio Couto Pereira",  "Curitiba(PR)", 31.596)
let estadio4 = new Estadio (54, "Estádio dos Aflitos",  "Recife (PE)", 22.856)
let estadio5 = new Estadio (55, "Estádio Raulino de Oliveira",  "Rio de Janeiro (RJ)", 18.230)
let estadio6 = new Estadio (56, "Estádio Olímpico Colosso da Lagoa",  "Erechim (RS)", 22.000)
let estadio7 = new Estadio (57, "Estádio Frasqueirão",  "Natal (RN)", 18.000)
let estadio8 = new Estadio (58, " Estádio Willie Davids",  "Maringá (PR)", 20.000)
let estadio9 = new Estadio (59, "Estádio Cícero Pompeu de Toledo",  "Morumbi (RJ)", 66.795)
let estadio10 = new Estadio (60, "Estádio Maracanã",  "Rio de Janeiro (RJ)", 78.838)
let estadio11 = new Estadio (61, "Estádio Independência",  " Belo Horizonte (MG)", 23.000)
let estadio12 = new Estadio (62, "Estádio Independência",  "Belo Horizonte (MG)", 23.000)
let estadio13 = new Estadio (63, "Estádio Beira-Rio ",  "Porto Alegre(RS)", 50.982)
let estadio14 = new Estadio (64, "Arena Castelão",  "Fortaleza(CE)", 63.904)
let estadio15 = new Estadio (65, "Estádio Rei Pelé",  "Maceió(AL)", 19.105)
let estadio16 = new Estadio (66, "Estádio Parque do Sabiá",  "Uberlândia(MG)", 53.350)

import Jogador from "./classes/Jogador.js"

import Jogo from "./classes/Jogo.js"

let jogo1 = new Jogo (67, "Estádio Raulino de Oliveira", "16h30" , "11 de abril de 2023", "90 minutos e acréscimos", "Volta Redonda" , "Bahia" , "1 a 2" , "8" , "Jogo 1 de 2" , "Luiz Flávio de Oliveira" );
console.log(jogo1);
let jogo2 = new Jogo (68, "Estádio Beira-Rio", "20h", "11 de abril de 2023", "90 minutos e acréscimos", "Internacional", "CSA", "2 a 1", "4", "Jogo 1 de 2", "Yuri Elino Ferreira da Cruz");
console.log(jogo2);
let jogo3 = new Jogo(69, "Estádio do Morumbi", "21h30", "11 de abril de 2023", "90 minutos e acréscimos" , "São Paulo", "Ituano", "0 a 0", "3", "Jogo 1 de 2", "Caio Max Augusto Vieira");
console.log(jogo3);
let jogo4 = new Jogo(70, "Couto Pereira", "19h", "12 de abril de 2023", "90 minutos e acréscimos", "Coritiba", "Sport Recife", "3 a 3", "6", "Jogo 1 de 2", "Leandro Pedro Vuaden");
console.log(jogo4);
let jogo5 = new Jogo (71, "Estádio do Maracanã", "19h30", "12 de abril de 2023", "90 minutos e acréscimos", "Fluminense" , "Paysandu", "3 a 0", "2" , "Jogo 1 de 2" , "Anderson Daronco");
console.log(jogo5);
let jogo6 = new Jogo (72, "Estádio Mangueirão", "21h30", "12 de abril de 2023", "90 minutos e acréscimos" , "Remo" , "Corinthians", "2 a 0" , "3" , "Jogo 1 de 2" , "Ramon Abatti Abel");
console.log(jogo6);
let jogo7 = new Jogo (73, "Colosso da Lagoa", "20h30", "12 de abril de 2023", "90 minutos e acréscimos", "Ypiranga", "Botafogo", "0 a 2", "5", "Jogo 1 de 2", "Paulo Cesar Zanovelli da Silva");
console.log(jogo7);
let jogo8 = new Jogo (74, "Estádio Willie Davids", "20h" , "13 de abril de 2023", "90 minutos e acréscimos", "Maringá", "Flamengo", "2 a 0", "3" , "Jogo 1 de 2" , "Flávio Rodrigues de Souza");
console.log(jogo8);
let jogo9 = new Jogo(75, "Estádio Santa Cruz" , "19h" , "11 de abril de 2023" , "90 minutos e acréscimos" , "Botafogo SP" ,  "Santos" , "0 a 2" , "7" , "Jogo 1 de 2" , "Bráulio da Silva Machado");
console.log(jogo9);               
let jogo10= new Jogo(76, "Arena Castelão", "21h30", "11 de abril de 2023", "90 minutos e acréscimos", "Fortaleza", "Águia de Marabá", "6 a 1", "1", "Jogo 1 de 2", "Luiz Cesar de Oliveira Magalhães");
console.log(jogo10);
let jogo11 = new Jogo(77, "Estádio General Raulino de Oliveira" , "16h30" , "12 de abril de 2023" , "90 minutos e acréscimos", "Nova Iguaçu" , "América-MG" , "1 a 2" , "10" , "Jogo 1 de 2" , "Diego Pombo Lopez");
console.log(jogo11);
let jogo12 = new Jogo(78, "Estádio Rei Pelé", "19h30", "12 de abril de 2023", "90 minutos e acréscimos", "CRB", "athletico-PR", "1 a 0", "8", "Jogo 1 de 2", "Leandro Pedro Vuaden");
console.log(jogo12);
let jogo13 = new Jogo(79,"Allianz Parque", "20h", "12 de abril de 2023", "90 minutos e acréscimos", "Palmeiras", "Tombense", "4 a 2", "4", "Jogo 1 de 2", "Bruno Arleu de Araujo");
console.log(jogo13);
let jogo14 = new Jogo(80, "Estádio Mineirão" , "21h30" , "12 de abril de 2023" , "90 minutos e acréscimos" , "Atlético-MG" , "  Brasil de Pelotas", "2 a 1" , "8" , "Jogo 1 de 2" , "Edina Alves Batista");
console.log(jogo14);
let jogo15 = new Jogo(81, "Estádio Eládio de Barros Carvalho", "19h", "13 de abril de 2023", "90 minutos e acréscimos", "Naútico", "Cruzeiro", "1 a 0", "7", "Jogo 1 de 2", "Marcelo de Lima Henrique");
console.log(jogo15);
let jogo16 = new Jogo(82, "Estádio Maria Lamas Farache", "21h30", "13 de abril de 2023", "90 minutos e acréscimos", "ABC" , "Grêmio", "0 a 2" , "0" , "Jogo 1 de 2", "Raphael Claus");
console.log(jogo16);

import Participacao from "./classes/Participação.js"

import Tecnico from "./classes/Tecnico.js"

import Time from "./classes/Time.js"
let time1 = new Time (01, "Botafogo-SP", "São Paulo")
let time2 = new Time (02, "Santos", "São Paulo")
let time3 = new Time (03, "América-MG", "Minas Gerais")
let time4 = new Time (04, "Nova Iguaçu-RJ", "Rio de Janeiro")
let time5 = new Time (05, "Sport", "Pernambuco")
let time6 = new Time (06, "Coritiba", "Parana")
let time7 = new Time (07, "Cruzeiro", "Minas Gerais")
let time8 = new Time (08, "Náutico", "Pernabuco")
let time9 = new Time (09, "Brasil-RS", "Rio Grande Do Sul")
let time10 = new Time (10, "Atlético-MG", "Minas Gerais")
let time11 = new Time (11, "Bahia", "Bahia")
let time12 = new Time (12, "Volta Redonda-RJ", "Rio De Janeiro")
let time13 = new Time (13, "Botafogo", "Rio De Janeiro")
let time14 = new Time (14, "Ypiranga-RS", "Rio Grande Do Sul")
let time15 = new Time (15, "Grêmio", "Rio Grande Do sul")
let time16 = new Time (16, "ABC-RN", "Rio Grande Do Norte")
let time17 = new Time (17, "Flamengo", "Rio De Janeiro")
let time18 = new Time (18, "Maringá-PR", "Parana")
let time19 = new Time (19, "Ituano-SP", "São Paulo")
let time20 = new Time (20, "São Paulo", "São Paulo")
let time21 = new Time (21, "Paysandu", "Para")
let time22 = new Time (22, "Fluminense", "Rio De Janeiro")
let time23 = new Time (23, "Corinthians", "São Paulo")
let time24 = new Time (24, "Remo", "Pará")
let time25 = new Time (25, "CSA", "Alagoas")
let time26 = new Time (26, "Internacional", "Rio Grande Do Sul")
let time27 = new Time (27, "Águia de Marabá-PA ", "Pará")
let time28 = new Time (28, "Fortaleza", "Ceará")
let time29 = new Time (29, "Athletico-PR", "Pará")
let time30 = new Time (30, "CRB", "Alagoas")
let time31 = new Time (31, "Tombense-MG ", "Minas Gerais")
let time32 = new Time (32, "Palmeiras", "São Paulo")


