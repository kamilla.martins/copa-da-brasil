export default class Jogo {
  constructor(id, local, horario, data, duracao, mandante, visitante, placar, cartoes, classificados, arbitro){
    this.id = id
    this.local = local
    this.horario = horario
    this.data = data
    this.duracao = duracao
    this.mandante = mandante
    this.visitante = visitante
    this.placar = placar
    this.cartoes = cartoes
    this.classificados = classificados
    this.arbitro = arbitro
  }
}
