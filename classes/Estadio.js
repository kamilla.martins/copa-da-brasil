export default class Estadio{
  constructor(id, nome, localizacao, capacidade){
    this.id = id;
    this.nome = nome;
    this.localizacao = localizacao;
    this.capacidade = capacidade;
  }
}
